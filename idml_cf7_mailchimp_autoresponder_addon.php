<?php
/*
 Plugin Name: Contact Form 7 AutoResponder Addon Plugin (fork by piogrek@gmail.com)
 Plugin URI:
 Description: Allows you to add your visitors to your Mailchimp AutoResponder list when they submit a message using Contact Form 7
 Author: Peter Grochowski <piogrek@gmail.com>
 Version: 1.5
 Author URI:
 */
define( 'CF7ADDON_PATH', dirname(__FILE__) . '/' );
if (!class_exists('MCAPI')) {
    include_once ( CF7ADDON_PATH . 'inc/MCAPI.class.php' );
}

add_action('plugins_loaded', 'idml_cf7_mailchimp_autoresponder_plugins_loaded_operations');

function idml_cf7_mailchimp_autoresponder_plugins_loaded_operations() {
    if(!function_exists('wpcf7_install')){
        add_action('admin_notices', 'cf7_addon_conflict_check');
        return;
    }
    // Add a menu for our options page
    add_action('admin_menu', 'cf7_autoresp_addon_add_page');
}

function cf7_addon_conflict_check(){
    echo '<div class="error fade"><p>Attention! You do not have the <a href="http://contactform7.com/" target="_blank">Contact Form 7 plugin</a> active! The Contact Form 7 AutoResponder Addon Plugin can only work if Contact Form 7 is active.</p></div>';
}

function cf7_autoresp_addon_add_page() {
    add_options_page( 'CF7 Mailchimp Addon', 'CF7 Mailchimp Addon', 'manage_options', 'cf7_autoresp_addon', 'cf7_autoresp_addon_option_page' );
}

// Draw the admin page
function cf7_autoresp_addon_option_page() {
    $lines           = 10;
    $mc_enabled      = 0;
    $mc_api_key      = '';
    $mc_list_name    = array();
    $mc_form_id      = array();
    $mc_custom_field = array();
    $errors          = false;
    //process form submission
    if (isset($_POST['Submit'])) {
        if ($_POST['mc-api'] != "") {
            $_POST['mc-api'] = filter_var($_POST['mc-api'], FILTER_SANITIZE_STRING);
            if ($_POST['mc-api'] == "") {
                $errors .= 'Please enter a valid api key.<br/><br/>';
            }
        } else {
            $errors .= 'Please enter your MailChimp API key.<br/>';
        }

        if (isset($_POST['mc-list-name']) && is_array($_POST['mc-list-name'])) {
            $_POST['mc-list-name'] = filter_var_array($_POST['mc-list-name'], FILTER_SANITIZE_STRING);
            if ($_POST['mc-list-name'][1] == "") {
                $errors .= 'Please enter a valid mailchimp list name.<br/>';
            }
        } else {
            $errors .= 'Please enter a MailChimp list name.<br/>';
        }
        if (isset($_POST['mc-form-id']) && is_array($_POST['mc-form-id'])) {
            $_POST['mc-form-id'] = filter_var_array($_POST['mc-form-id'], FILTER_SANITIZE_NUMBER_INT);
            if ($_POST['mc-form-id'][1] == "") {
                $errors .= 'Please enter a valid contact-form-7 ID.<br/>';
            }
        } else {
            $errors .= 'Please enter a contact-form-7 ID.<br/>';
        }

        if (!$errors) {
            if (isset($_POST['enable-mc'])) {
                $mc_enabled = 1;
            } else {
                $mc_enabled = 0;
            }

            if (isset($_POST['disable-double-opt'])) {
                $mc_disable_double_opt = false; //this means that we want to disable double optin emails
            } else {
                $mc_disable_double_opt = true;
            }

            //add the data to the wp_options table
            $options = array(
                'mc_enabled'            => $mc_enabled,
                'mc_api_key'            => $_POST['mc-api'],
                'mc_list_name'          => $_POST['mc-list-name'],
                'mc_form_id'            => $_POST['mc-form-id'],
                'mc_custom_field'       => $_POST['mc-custom-field'],
                'mc_mail_field_mapping' => $_POST['mc-mail-field-mapping'],
                'mc_name_field_mapping' => $_POST['mc-name-field-mapping'],
                'mc_disable_double_opt' => $_POST['disable-double-opt']
            );
            // var_dump($options);die();
            update_option('cf7_autoresp_addon', $options); //store the results in WP options table
            echo '<div id="message" class="updated fade">';
            echo '<p>Settings Saved</p>';
            echo '</div>';
        }
        else
        {
            echo '<div style="color: red">' . $errors . '<br/></div>';
        }
    }
    if (get_option('cf7_autoresp_addon')) {
        $mc_settings           = get_option('cf7_autoresp_addon');
        $mc_enabled            = $mc_settings['mc_enabled'];
        $mc_api_key            = $mc_settings['mc_api_key'];
        $mc_list_name          = $mc_settings['mc_list_name'];
        $mc_form_id            = $mc_settings['mc_form_id'];
        $mc_custom_field       = $mc_settings['mc_custom_field'];
        $mc_mail_field_mapping = $mc_settings['mc_mail_field_mapping'];
        $mc_name_field_mapping = $mc_settings['mc_name_field_mapping'];
        $mc_disable_double_opt = $mc_settings['mc_disable_double_opt'];
    }
    ?>
<div class="wrap">
<div id="poststuff"><div id="post-body">
    <h2>Contact Form 7 AutoResponder Addon</h2>
    <div class="postbox">
        <h3>Enter Your MailChimp Account Details</h3>
        <form action="<?php echo $_SERVER["REQUEST_URI"]; ?>" method="POST" onsubmit="">
            <table class="form-table">
                <tr valign="top">
                    <th scope="row"><label for="enable-mc"> Enable Mailchimp List
                    Insertion: </label></th>
                    <td><input type="checkbox" name="enable-mc"
                    <?php if($mc_enabled) echo ' checked="checked"'; ?> /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="MCAPIKey"> Enter MailChimp API Key:</label>
                    </th>
                    <td><input size="50" name="mc-api" value="<?php echo $mc_api_key; ?>" /></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="disable-double-opt"> Disable Double Opt-in Email: </label></th>
                    <td><input type="checkbox" name="disable-double-opt"
                    <?php if($mc_disable_double_opt) echo ' checked="checked"'; ?> />
                    <span class="description"> When enabling this checkbox the plugin will ask MailChimp NOT to send a confirmation (double opt-in) email</span></td>
                </tr>
                <tr><td colspan="2"><hr></td></tr>
                <tr>
                    <td colspan="2" >
                        <table>
                            <tr>
                                <th>Mailchimp list name</th>
                                <th>Contact Form 7 - form ID</th>
                                <th>Boolean field to set</th>
                                <th>Mail field mapping</th>
                                <th>Name field mapping</th>
                            </tr>

                            <?php for($i=1;$i<=$lines;$i++): ?>
                            <tr>
                                <td><input size="25" name="mc-list-name[<?php echo $i ?>]"    value="<?php echo $mc_list_name[$i]; ?>" ></td>
                                <td><input size="25" name="mc-form-id[<?php echo $i ?>]"      value="<?php echo $mc_form_id[$i]; ?>" ></td>
                                <td><input size="25" name="mc-custom-field[<?php echo $i ?>]" value="<?php echo $mc_custom_field[$i]; ?>" ></td>
                                <td><input size="25" name="mc-mail-field-mapping[<?php echo $i ?>]" value="<?php echo $mc_mail_field_mapping[$i]; ?>" ></td>
                                <td><input size="25" name="mc-name-field-mapping[<?php echo $i ?>]" value="<?php echo $mc_name_field_mapping[$i]; ?>" ></td>
                            </tr>
                            <?php endfor ?>
                            <!-- <tr>
                                <td><input size="25" name="mc-list-name[2]" value="<?php echo $mc_list_name[2]; ?>" ></td>
                                <td><input size="25" name="mc-form-id[2]" value="<?php echo $mc_form_id[2]; ?>" ></td>
                                <td><input size="25" name="mc-custom-field[2]" value="<?php echo $mc_custom_field[2]; ?>" ></td>
                            </tr>
                            <tr>
                                <td><input size="25" name="mc-list-name[3]" value="<?php echo $mc_list_name[3]; ?>" ></td>
                                <td><input size="25" name="mc-form-id[3]" value="<?php echo $mc_form_id[3]; ?>" ></td>
                                <td><input size="25" name="mc-custom-field[3]" value="<?php echo $mc_custom_field[3]; ?>" ></td>
                            </tr>
                            <tr>
                                <td><input size="25" name="mc-list-name[4]" value="<?php echo $mc_list_name[4]; ?>" ></td>
                                <td><input size="25" name="mc-form-id[4]" value="<?php echo $mc_form_id[4]; ?>" ></td>
                                <td><input size="25" name="mc-custom-field[4]" value="<?php echo $mc_custom_field[4]; ?>" ></td>
                            </tr> -->
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <input name="Submit" type="submit" value="Save Settings"
            class="button-primary" />
    </form>
    </div></div>
    <hr>
    </div>
        <?php
}

add_action( 'wpcf7_before_send_mail', 'contact7addonfunc' ); //use the cf7 hook

function contact7addonfunc($cf7) {
    $mailchimp_settings          = get_option('cf7_autoresp_addon');
    $mc_api                      = $mailchimp_settings['mc_api_key']; //get api key from DB
    $mc_list_array               = $mailchimp_settings['mc_list_name']; //get list name from DB
    $mc_form_id_array            = $mailchimp_settings['mc_form_id']; // form ids
    $mc_custom_field_array       = $mailchimp_settings['mc_custom_field']; // form ids
    $mc_mail_field_mapping_array = $mailchimp_settings['mc_mail_field_mapping']; // form ids
    $mc_name_field_mapping_array = $mailchimp_settings['mc_name_field_mapping']; // form ids
    $mc_enabled                  = $mailchimp_settings['mc_enabled']; //get global checkbox value for this feature
    $mc_disable_double_opt       = $mailchimp_settings['mc_disable_double_opt'];

    //the following few lines will check if an "opt-in" checkbox has been added to the CF7 form
    $optin_box_exists = false; //assume by default a CF7 form will not have an "mc-subscribe" checkbox

    $form_id        = $cf7->posted_data['_wpcf7']; //get form ID so we can search the post meta data table
    $form_meta_data = get_post_meta($form_id); //get the form meta-data so we can see if there is "mc-subscribe" checkbox

    // search for the form ID in plugin configuration
    $key = array_search($form_id, $mc_form_id_array);
    $updateRecord = false;

    // if form not found - stop and exit - it's not an error
    if ($key === false) {
        return;
    }

    // if list not found - stop and exit
    if (!isset($mc_list_array[$key])) {
        error_log( "Unable to load mailchimp list for form id #{$form_id}!\n", 3, dirname( __FILE__ ).'/cf7_autoresp.log' );
        return;
    }
    $mc_list = $mc_list_array[$key];
    $mc_custom_field = $mc_custom_field_array[$key];
    $mc_mail_field_mapping = $mc_mail_field_mapping_array[$key];
    $mc_name_field_mapping = $mc_name_field_mapping_array[$key];

    $form_details = $form_meta_data['_form'][0];

    //  check if subscribe option exists on the form
    if (strpos($form_details, 'mc-subscribe') !== false) {
        $optin_box_exists = true;
    }

    if ($mc_enabled) {
        if (($optin_box_exists) && (!$cf7->posted_data['mc-subscribe'])) {
            return; //do not subscribe if user has left opt-in box disabled
        }

        // foreach($cf7->scanned_form_tags as $item) {
        //     if($item['type'] == 'submit') {
        //         if(strpos($item['raw_values'][0],"|")) {
        //             //error_log(print_r($item['raw_values'], true), 3, dirname( __FILE__ ).'/cf7_post.log' );
        //             $res = explode("|",$item['raw_values'][0]); //get the listname
        //             $mc_list = $res[1];
        //             break;
        //         }
        //     }
        // }
        $email = $cf7->posted_data[$mc_mail_field_mapping]; //get the submitted email address from CF7
        $firstname = $cf7->posted_data[$mc_name_field_mapping];


        if (array_key_exists('your-last-name', $cf7->posted_data)) {
            $lastname = $cf7->posted_data["your-last-name"]; //in case someone creates this field
        } else {
            $lastname = '';
        }
        $mergeVars = array(
            'FNAME'=>$firstname,
            'LNAME'=>$lastname,
        );

        if (!empty($mc_custom_field)) {
            $mergeVars[$mc_custom_field] = 1;
        }

        $email_type = 'html'; //for some reason I need this in order to get the API to identify the disable_optin variable

        $api = new MCAPI($mc_api);
        $yourlists = $api->lists();
        foreach ($yourlists['data'] as $list){
            if($list['name'] == $mc_list) {
                error_log("Subscribing {$email} to the {$list['name']} list\n", 3, dirname( __FILE__ ).'/cf7_autoresp.log');
                $retval = $api->listSubscribe( $list['id'], $email, $mergeVars, $email_type, $mc_disable_double_opt, $updateRecord, false, false);   //add subscriber to mailchimp
                // listSubscribe(
                //     string id,
                //     string email_address,
                //     array merge_vars,
                //     string email_type,
                //     bool double_optin,
                //     bool update_existing,
                //     bool replace_interests,
                //     bool send_welcome)
            }
            if ($api->errorCode){
                error_log( "Unable to load listSubscribe()!\n", 3, dirname( __FILE__ ).'/cf7_autoresp.log' );
                error_log( "\tCode=".$api->errorCode."\n", 3, dirname( __FILE__ ).'/cf7_autoresp.log' );
                error_log( "\tMsg=".$api->errorMessage."\n", 3, dirname( __FILE__ ).'/cf7_autoresp.log' );
                return;
            } else {
            }
        }
    }
}
?>